package com.genius.discordbot.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.genius.discordbot.permission.Permission;
import com.genius.discordbot.permission.PermissionUtils;

public class PermissionTest {
	
	@Test
	public void testPermission() {
		boolean isHigher = PermissionUtils.getHigherPermission(Permission.ADMINISTRATOR, Permission.GUEST);
		assertTrue(isHigher);
	}

}
