package com.genius.discordbot.permission;

public enum Permission {
	
	ADMINISTRATOR(5, 295645828079419393l), MODERATOR(4, 295645947332132864l), MEDIATOR(3, 295646098188664832l), EDITOR(2, 295645664896090113l), CONTRIBUTOR(1, 295646007511744512l), GUEST(0,  308678122184638464l);

	private int hierarchy;
	private long id;
	
	private Permission(int hierarchy, long id) {
		this.hierarchy = hierarchy;
		this.id = id;
	}
	
	public int getHierarchy() {
		return hierarchy;
	}
	
	public long getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return this.name().charAt(0) + this.name().toLowerCase().substring(1, this.name().length());
	}

}
