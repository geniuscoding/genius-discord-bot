package com.genius.discordbot.api;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class APIConnection {
	
	private String token;
	private final Logger logger;
	
	public APIConnection(final String token) {
		this.token = token;
		this.logger = LoggerFactory.getLogger(this.getClass());
	}
	
	public JSONObject getRequest(final String endpoint) throws IOException {
		URL obj = new URL("https://api.genius.com/" + endpoint);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "CompuServe Classic/1.22");
		con.setRequestProperty("Authorization", "Bearer " + token);
		con.setRequestProperty("Accept", "application/json");

		int responseCode = con.getResponseCode();
		if (responseCode >= 200 && responseCode < 300) {
			StringBuilder builder = new StringBuilder();
		    try(Scanner s = new Scanner(new BufferedInputStream(con.getInputStream()))) {
		    	while(s.hasNextLine()) {
		    		builder.append(s.nextLine());
		    	}
		    }
		    return new JSONObject(builder.toString());
		} else {
			logger.warn("Could not fetch response for Genius API call for endpoint \"" + endpoint + "\": " + responseCode);
			return null;
		}
	}
}
