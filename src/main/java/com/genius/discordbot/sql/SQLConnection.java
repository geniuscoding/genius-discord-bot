package com.genius.discordbot.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;

import sx.blah.discord.handle.obj.IUser;

public class SQLConnection {
	
    private static final String DB_URL = "jdbc:mysql://localhost:3306/geniusdb?autoReconnect=true&useUnicode=yes";
    private static final String USER = "geniusbot";
    private final Logger logger;
	private Connection connection;
	private GeniusBot bot;
	private String pass;
    private boolean failed = false;
	
	public SQLConnection(final GeniusBot bot, final String pass) {
		this.bot = bot;
		this.pass = pass;
		this.logger = LoggerFactory.getLogger(this.getClass());
		failed = !connectToMysql();
		if(!failed) {
			logger.info("Successfully established connection to MySQL server.");
		} else {
			logger.warn("SQL connection could not be established.");
		}
	}

	private boolean connectToMysql() {
		try {
			if((connection = DriverManager.getConnection(DB_URL, USER, pass)) == null) {
				return false;
			}
			return true;
		} catch (SQLException e){
			logger.warn("Could not establish SQL connection: " + e.getMessage());
			logger.warn("SQLState: " + e.getSQLState() + " / VendorError: " + e.getErrorCode());
			return false;
		}
	}
	
	public String getIQ(final String username) throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT displaytotalIQ FROM iqtrend WHERE username = '" + getGeniusID(username) + "' ORDER BY fetchdatetime DESC LIMIT 1");
	    if (rs.next()){
        	return rs.getString("displaytotalIQ");
        }
		return null;
	}
	
	public List<IQTableUser> getEntireIQ(final String username) throws SQLException {
		List<IQTableUser> li = new ArrayList<>();
		ResultSet rs = connection.createStatement().executeQuery("SELECT totaliq, fetchdatetime FROM iqtrend WHERE username = '" + getGeniusID(username) + "'");
	    while (rs.next()){
	    	li.add(new IQTableUser(rs.getString(1), rs.getString(2)));
	    }
		return li;
	}
	
	public List<IQLeaderboardUser> getLeaderboardsGraph() throws SQLException {
		List<IQLeaderboardUser> li = new ArrayList<>();
		ResultSet rs = connection.createStatement().executeQuery("SELECT username, totalIQ As aktuell, (SELECT totalIQ FROM iqtrend WHERE username = iq.username AND DATE(fetchdatetime) = subdate(curdate(), 1) LIMIT 1) AS gestern, totalIQ - (SELECT totalIQ FROM iqtrend WHERE username = iq.username AND DATE(fetchdatetime) = subdate(curdate(), 1) LIMIT 1) AS Differenz FROM iqtrend AS iq WHERE DATE(fetchdatetime) = curdate() ORDER BY Differenz DESC LIMIT 10;");
	    while (rs.next()){
	    	li.add(new IQLeaderboardUser(rs.getString(1), rs.getString(2)));
	    }
		return li;
	}
	
	public List<IQTableUser> getTabledData(final String username) throws SQLException {
		List<IQTableUser> li = new ArrayList<>();
		ResultSet rs = connection.createStatement().executeQuery("SELECT TRUNCATE((totaliq / 1000), 0) * 1000, DATE(fetchdatetime), totaliq FROM `iqtrend` WHERE `username` = \"" + getGeniusID(username) + "\" GROUP BY (TRUNCATE((totaliq / 1000), 0)) * 1000 ORDER BY fetchdatetime DESC LIMIT 15;");
	    while (rs.next()){
	    	li.add(new IQTableUser(String.format("%,d", rs.getInt(1)), new SimpleDateFormat("dd. MMMM yyyy").format(rs.getDate(2))));
	    }
		return li;
	}
	
	public String getGraph(final String username) throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT userid FROM graph WHERE id = '" + getGeniusID(username) + "'");
	    if (rs.next()){
	    	return "https://larsbutnotleast.xyz/geniusgraph/users/" + rs.getString("userid") + "/latest.jpg";
	    }
		return null;
	}
	
	public String getGeniusID(final String username) throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT id FROM graph WHERE username = '" + username + "'");
	    if (rs.next()){
	    	return rs.getString("id");
	    }
		return null;
	}
	
	public String getAuthToken(final String username) throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT token FROM graph WHERE username = '" + username + "'");
	    if (rs.next()){
	    	return rs.getString("token");
	    }
		return null;
	}
	
	public List<String> getAllUsers() throws SQLException {
		List<String> li = new ArrayList<>();
		ResultSet rs = connection.createStatement().executeQuery("SELECT username FROM graph WHERE username != '' AND active = 1 AND username != '???'");
	    while (rs.next()){
	    	li.add(rs.getString("username"));
	    }
	    Collections.sort(li, (s1, s2) -> s1.compareToIgnoreCase(s2));
	    return li;
    }
	
	public int countRegisteredUsers() throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT COUNT(*) FROM graph WHERE active = 1");
	    if (rs.next()){
	    	try {
	    		return Integer.parseInt(rs.getString(1));
	    	} catch(NumberFormatException e) {
	    		logger.warn("Unable to count registered users, unexpected response from DB: " + e.getMessage());
	    		e.printStackTrace();
	    		return 0;
	    	}
	    }
		return 0;
	}
	
	public long countFetchedIQs() throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT COUNT(*) FROM iqtrend");
	    if (rs.next()){
	    	try {
	    		return Long.parseLong(rs.getString(1));
	    	} catch(NumberFormatException e) {
	    		logger.warn("Unable to count fetched IQs, unexpected response from DB: " + e.getMessage());
	    		e.printStackTrace();
	    		return 0l;
	    	}
	    }
		return 0l;
	}
	
	public String getSettingValue(final int idx) throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT val FROM settings WHERE idx = " + idx);
	    if (rs.next()){
	    	return rs.getString(1);
	    }
		return null;
	}
	
	public int getTag(final String description) throws SQLException {
		ResultSet rs = connection.createStatement().executeQuery("SELECT tag FROM tags WHERE description = '" + description + "'");
	    if (rs.next()){
	    	return rs.getInt("tag");
	    }
		return 0;
	}
	
	// Noch ben�tigt?
	@Deprecated
	public boolean checkGeniusUserPermission(IUser discorduser, String geniususer) throws SQLException {
		if(discorduser.hasRole(bot.getClient().getRoleByID(GeniusDiscordUtility.getAdministratorRole()))) return true;
	    return connection.createStatement().executeQuery("SELECT id FROM privileges WHERE discordID = '" + String.valueOf(discorduser.getLongID()) + "' and GeniusUsername = '" + geniususer + "'").next() ? true : false;
	}
	
	@Deprecated
	public void insertGeniusUserPermission(IUser discorduser, String geniususer) throws SQLException {
		connection.createStatement().execute("INSERT INTO privileges(discordID, GeniusUsername) VALUES ('" + String.valueOf(discorduser.getLongID()) + "', '" + geniususer + "')");
	}
	
	public Connection getConnection() {
		return connection;
	}

	public boolean hasFailed() {
		return failed;
	}

}
