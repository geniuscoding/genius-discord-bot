package com.genius.discordbot.sql;

public class IQTableUser {

	private final String iq;
	private final String date;

	public IQTableUser(final String iq, final String date) {
		this.iq = iq;
		this.date = date;
	}
	
	public String getIQ() {
		return iq;
	}

	public String getDate() {
		return date;
	}

}
