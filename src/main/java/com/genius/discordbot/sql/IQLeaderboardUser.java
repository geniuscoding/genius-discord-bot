package com.genius.discordbot.sql;

public class IQLeaderboardUser {

	private final String user;
	private final String iqdifference;

	public IQLeaderboardUser(final String user, final String iqdifference) {
		this.user = user;
		this.iqdifference = iqdifference;
	}
	
	public String getUser() {
		return user;
	}

	public String getIQDifference() {
		return iqdifference;
	}

}
