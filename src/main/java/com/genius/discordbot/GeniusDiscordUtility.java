package com.genius.discordbot;

import com.genius.discordbot.permission.Permission;

public final class GeniusDiscordUtility {

	/*
	 * Allgemeine Konstanten auf dem GeniusDeu Discord-Server.
	 * Dieser Bot wurde nicht daf�r konzipiert, auf anderen Servern zu funktionieren
	 * � deshalb verl�sst er alle Server, die nicht die ID 285822817155678208 haben.
	 */
	
	private static Config cfg;
	
	static {
		cfg = GeniusBot.getConfig();
	}
	
	// General 
	
	public static long getGeniusGuildID() {
		return 285822817155678208l;
	}
	
	public static char getDelimiter() {
		return (char) cfg.getOrSet("bot.delimiter", '!');
	}
	
	// Channels
	
	public static long getEingangshalleChannelID() {
		return (long) cfg.getOrSet("channel.eingangshalle", 285822817155678208l);
	}
	
	public static long getGeneralChannelID() {
		return (long) cfg.getOrSet("channel.general", 319802683521236992l);
	}
	
	public static long getBotChannelID() {
		return (long) cfg.getOrSet("channel.bot", 297073378433695744l);
	}
	
	// Roles
	
	public static long getAdministratorRole() {
		return Permission.ADMINISTRATOR.getId();
	}
	
	public static long getModeratorRole() {
		return Permission.MODERATOR.getId();
	}
	
	public static long getMediatorRole() {
		return Permission.MEDIATOR.getId();
	}
	
	public static long getEditorRole() {
		return Permission.EDITOR.getId();
	}
	
	public static long getContributorRole() {
		return Permission.CONTRIBUTOR.getId();
	}
	
	public static long getGuestRole() {
		return Permission.GUEST.getId();
	}
	
}
