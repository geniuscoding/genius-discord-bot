package com.genius.discordbot;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blogspot.debukkitsblog.util.FileStorage;

public class Config extends FileStorage {
	
	private final Logger logger;

	public Config(File file) throws IOException, IllegalArgumentException {
		super(file);
		logger = LoggerFactory.getLogger(Config.class);
	}
	
	public Object getOrSet(String key, Object defaultValue) {
		if(this.hasKey(key)) {
			return this.get(key);
		} else {
			this.store(key, defaultValue);
			return defaultValue;
		}
	}

	@Override
	public void store(String key, Object o) {
		try {
			super.store(key, o);
		} catch(IOException e) {
			logger.error("Access to config denied or config corrupted: " + e.getMessage());
			e.printStackTrace();
			System.exit(-2);
		}
	}

}
