package com.genius.discordbot;

import java.lang.reflect.InvocationTargetException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genius.discordbot.api.APIConnection;
import com.genius.discordbot.commands.CommandSubscriber;
import com.genius.discordbot.commands.DiscordCommand;
import com.genius.discordbot.commands.DiscordCommandInfo;
import com.genius.discordbot.events.DiscordEvent;
import com.genius.discordbot.events.EventSubscriber;
import com.genius.discordbot.sql.SQLConnection;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.util.DiscordException;

public final class GeniusBot implements Runnable {
	
	private final String c_token;
	private final String g_token;
	private final String db_token;
	private IDiscordClient client;
	private final Logger logger;
	private ZonedDateTime launchTimestamp;
	private SQLConnection sqlconnection;
	private APIConnection apiconnection;
	
	private List<DiscordEvent<? extends Event>> events = new ArrayList<>();
	private BiMap<DiscordCommandInfo, DiscordCommand> commands = HashBiMap.create();
	
	private static Config config;

	GeniusBot(final String c_token, final String g_token, final String db_token, final Config config) {
		this.c_token = c_token;
		this.g_token = g_token;
		this.db_token = db_token;
		this.logger = LoggerFactory.getLogger(GeniusBot.class);
		this.launchTimestamp = ZonedDateTime.now();
		this.sqlconnection = new SQLConnection(this, db_token);
		this.apiconnection = new APIConnection(g_token);
		GeniusBot.config = config;
	}

	@Override
	public void run() {
		ClientBuilder builder = new ClientBuilder();
		builder.withToken(this.c_token);
		try {
			this.client = builder.build();
			registerEventListeners();
			registerCommands();
			this.client.login();
		} catch (DiscordException e) {
			logger.error("Could not authenticate Discord Bot: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void registerEventListeners() {
		Reflections r = new Reflections("com.genius.discordbot.events");
		Set<Class<?>> clazzes = r.getTypesAnnotatedWith(EventSubscriber.class);
		for(Class<?> clazz : clazzes) {
			if(!DiscordEvent.class.isAssignableFrom(clazz)) {
				logger.warn("Could not register event \"" + clazz.getSimpleName() + "\": Class does not extend EventListener.");
				break;
			}
			try {
				Object listener = clazz.getConstructors()[0].newInstance(this);
				if(listener instanceof DiscordEvent<?>) {
					events.add((DiscordEvent<?>) listener);
					this.client.getDispatcher().registerListener((DiscordEvent<?>) listener);
					logger.info("Successfully registered event \"" + clazz.getSimpleName() + "\"");
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | SecurityException e) {
				logger.warn("Could not register event \"" + clazz.getSimpleName() + "\": " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	private void registerCommands() {
		Reflections r = new Reflections("com.genius.discordbot.commands");
		Set<Class<?>> clazzes = r.getTypesAnnotatedWith(CommandSubscriber.class);
		for(Class<?> clazz : clazzes) {
			if(!DiscordCommand.class.isAssignableFrom(clazz)) {
				logger.warn("Could not register command \"" + clazz.getSimpleName() + "\": Class does not extend CommandListener.");
				break;
			}
			try {
				Object command = clazz.getConstructors()[0].newInstance(this);
				if(command instanceof DiscordCommand) {
					CommandSubscriber cs = clazz.getAnnotation(CommandSubscriber.class);
					commands.put(new DiscordCommandInfo(cs.syntax().split(" ")[0], cs.syntax(), cs.help(), cs.permission()), (DiscordCommand) command);
					logger.info("Successfully registered command \"" + clazz.getSimpleName() + "\"");
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | SecurityException e) {
				logger.warn("Could not register command \"" + clazz.getSimpleName() + "\": " + e.getMessage());
				e.printStackTrace();
			}
		}
 	}
	
	public String getG_token() {
		return g_token;
	}

	public String getDb_token() {
		return db_token;
	}

	public IDiscordClient getClient() {
		return client;
	}

	public List<DiscordEvent<? extends Event>> getEvents() {
		return events;
	}

	public BiMap<DiscordCommandInfo, DiscordCommand> getCommands() {
		return commands;
	}
	
	public ZonedDateTime getLaunchTimestamp() {
		return launchTimestamp;
	}
	
	public SQLConnection getSqlconnection() {
		return sqlconnection;
	}
	
	public APIConnection getApiconnection() {
		return apiconnection;
	}
	
	public static Config getConfig() {
		return config;
	}
	
}
