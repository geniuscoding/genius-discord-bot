package com.genius.discordbot;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateInitiatorPooler implements Runnable {

	private final GeniusBot bot;
	private final Logger logger;
	
	UpdateInitiatorPooler(final GeniusBot bot) {
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.bot = bot;
	}
	
	@Override
	public void run() {
		try {	
			while (true) {
				Path filePath = Paths.get("/home/lars/Skripts/geniusbot/", "update.me");
				File f = filePath.toFile();
				if (f.exists()) {
					f.delete();
					bot.getClient()
					.getGuildByID(GeniusDiscordUtility.getGeniusGuildID())
					.getChannelByID(GeniusDiscordUtility.getBotChannelID())
					.sendMessage("Ein Update wurde initiiert. Der Bot wurde erfolgreich beendet - bis gleich :wave:");
					try {
						bot.getSqlconnection().getConnection().close();
					} catch (SQLException | NullPointerException ex) {
						logger.warn("Unable to close SQL connection gracefully, shutting down forcefully.");
						logger.warn(ex.getMessage());
					}
					bot.getClient().logout();
					System.exit(2);
				}
				Thread.sleep(2000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
