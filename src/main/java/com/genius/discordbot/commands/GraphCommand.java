package com.genius.discordbot.commands;

import java.sql.SQLException;
import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

@CommandSubscriber(syntax = "graph <Nutzer>", help = "Zeigt den IQ-Graph des angegebenen Nutzers an.", permission = Permission.CONTRIBUTOR)
public class GraphCommand extends DiscordCommand {

	public GraphCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if(args.size() == 1) {
			String g;
			try {
				g = this.getBot().getSqlconnection().getGraph(args.get(0));
				e.getChannel().sendMessage(g != null ? g : ":x: Nutzer existiert nicht! Bitte auf https://larsbutnotleast.xyz/geniusgraph/ registrieren.");
			} catch (SQLException ex) {
				e.getChannel().sendMessage(":x: Ein Fehler ist aufgetreten: " + ex.getMessage());
			}
		} else {
			sendInvalidArgsMessage(e);
		}
	}

}
