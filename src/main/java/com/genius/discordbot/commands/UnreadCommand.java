package com.genius.discordbot.commands;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONObject;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.api.APIConnection;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

@CommandSubscriber(syntax = "unread <username>", help = "Gibt Informationen �ber ungelesene Benachrichtigungen eines Genius(Graph) Nutzers.", permission = Permission.CONTRIBUTOR)
public class UnreadCommand extends DiscordCommand {

	public UnreadCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if (args.size() == 1) {
			String user = args.get(0);
			String token;
			try {
				token = this.getBot().getSqlconnection().getAuthToken(user);
			} catch (SQLException | NullPointerException ex) {
				e.getChannel().sendMessage(":x: Ein Fehler ist aufgetreten: " + ex.getMessage());
				return;
			}
			if (token != null) {
				JSONObject response;
				try {
					response = new APIConnection(token).getRequest("account");
				} catch (IOException ex) {
					e.getChannel().sendMessage(":x: Ein Fehler ist aufgetreten: " + ex.getMessage());
					return;
				}
				if (response != null) {
					EmbedBuilder embed = new EmbedBuilder();
					JSONObject uobj = response.getJSONObject("response").getJSONObject("user");
					embed.withAuthorIcon(uobj.getJSONObject("avatar").getJSONObject("thumb").getString("url"));
					embed.withAuthorName(uobj.getString("name").replaceAll("[^\\p{Print}]", ""));
					embed.withAuthorUrl(uobj.getString("url"));
					embed.appendField("Forum", String.valueOf(uobj.getInt("unread_groups_inbox_count")), false);
					embed.appendField("Newsfeed", String.valueOf(uobj.getInt("unread_newsfeed_inbox_count")), false);
					embed.appendField("Benachrichtigungen", String.valueOf(uobj.getInt("unread_main_activity_inbox_count")), false);
					embed.appendField("Nachrichten", String.valueOf(uobj.getInt("unread_messages_count")), false);
					int[] rolecolor = getColorForRole(uobj.getString("human_readable_role_for_display"));
					embed.withColor(rolecolor[0], rolecolor[1], rolecolor[2]);
					RequestBuffer.request(() -> e.getChannel().sendMessage(embed.build()));
				}
			} else {
				e.getChannel().sendMessage(":x: Dieser Nutzer existiert nicht!");
			}
		} else {
			sendInvalidArgsMessage(e);
		}
	}
	
	private int[] getColorForRole(String role) {
		switch(role) {
		case "Staff":
			return new int[] {176, 196, 222};
		case "Verified Artist":
			return new int[] {39, 241, 69};
		case "Moderator":
			return new int[] {118, 137, 232};
		case "Mediator":
			return new int[] {255, 81, 140};
		case "Editor":
			return new int[] {255, 255, 100};
		case "Contributor":
			return new int[] {255, 255, 255};
		default:
			return null;
		}
	}
	
}