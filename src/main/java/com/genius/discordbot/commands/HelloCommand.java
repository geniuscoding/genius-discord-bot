package com.genius.discordbot.commands;

import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

@CommandSubscriber(syntax = "hallo", help = "Sagt dir Hallo!", permission = Permission.CONTRIBUTOR)
public class HelloCommand extends DiscordCommand {

	public HelloCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(final MessageReceivedEvent e, final List<String> args) {
		if(args.size() == 0) {
			e.getChannel().sendMessage(e.getAuthor().mention() + ": Hallo!");
		} else {
			sendInvalidArgsMessage(e);
		}
	}

}
