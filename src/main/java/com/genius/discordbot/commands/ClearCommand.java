package com.genius.discordbot.commands;

import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.MessageHistory;
import sx.blah.discord.util.RateLimitException;

@CommandSubscriber(syntax = "clear <Anzahl>", help = "L�scht die letzten <Anzahl> Nachrichten in einem Channel.", permission = Permission.MODERATOR)
public class ClearCommand extends DiscordCommand {

	public ClearCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if(args.size() == 1) {
			int mcount;
			try {
				if((mcount = Integer.parseInt(args.get(0))) < 1) {
					e.getChannel().sendMessage(":x: Es k�nnen nicht weniger als eine Nachricht gel�scht werden!");
					return;
				}
			} catch(NumberFormatException ex) {
				e.getChannel().sendMessage(":x: Ung�ltige Nachrichtenanzahl!");
				return;
			}
			e.getMessage().delete();
			IMessage msg = e.getChannel().sendMessage(":white_check_mark: Hole gesamten Nachrichtenverlauf, dies k�nnte eine Weile dauern ...");
			new Thread(() -> {
				try { Thread.sleep(8000); } catch(InterruptedException ex) {}
				msg.delete();
			}, "IMessage Watchdog").start();
			new Thread(new BulkDeleteRunnable(e, mcount), "BulkDelete Thread").start();
		} else {
			sendInvalidArgsMessage(e);
		}
	}
	
	private class BulkDeleteRunnable implements Runnable {

		private MessageReceivedEvent mrec;
		private int mcount;
		
		BulkDeleteRunnable(final MessageReceivedEvent mrec, final int mcount) {
			this.mrec = mrec;
			this.mcount = mcount;
		}
		
		@Override
		public void run() {
			synchronized(ClearCommand.this) {
				MessageHistory mhist = mrec.getChannel().getFullMessageHistory();
				for(int i = 0; i < mcount; i++) {
					IMessage m = mhist.get(i);
					if(m.getContent().equals(":white_check_mark: Hole gesamten Nachrichtenverlauf, dies k�nnte eine Weile dauern ...") && m.getAuthor().equals(ClearCommand.this.getBot().getClient().getOurUser())) {
						mcount++;
						continue;
					}
					try {
						m.delete();
					} catch(RateLimitException ex) {
						try {
							long de = ex.getRetryDelay();
							Thread.sleep(de + 10);
							m.delete();
						} catch(InterruptedException iex) {
							iex.printStackTrace();
						}
					}
				}
			}
		}
		
	}

}
