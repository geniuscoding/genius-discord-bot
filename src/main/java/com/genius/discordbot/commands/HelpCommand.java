package com.genius.discordbot.commands;

import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;
import com.genius.discordbot.permission.Permission;
import com.google.common.collect.BiMap;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.IRequest;

@CommandSubscriber(syntax = "help", help = "Zeigt alle Befehle des Bots.", permission = Permission.GUEST)
public class HelpCommand extends DiscordCommand {

	public HelpCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if(args.size() == 0) {
			EmbedBuilder embed = new EmbedBuilder();
			BiMap<DiscordCommandInfo, DiscordCommand> map = this.getBot().getCommands();
			embed.withColor(255, 255, 100);
			embed.withAuthorIcon(this.getBot().getClient().getOurUser().getAvatarURL());
			embed.withAuthorName(this.getBot().getClient().getOurUser().getName());
			embed.withAuthorUrl("https://genius.com/Genius-deutschland-werde-teil-der-community-von-genius-deutschland-annotated");
			embed.withDescription("Dieser Bot wurde von einer Gruppe freiwilligen Entwicklern im Genius Deutschland Editor-Team"
					+ " f�r den Genius Deutschland Discord-Server erstellt. Sein Ziel ist es, den Nutzern des Discords Funktionen"
					+ " der Web-App direkt zur Verf�gung zu stellen und f�r Unterhaltung auf verschiedenen Arten zu sorgen.\n"
					+ "Der Bot wurde mit Hilfe von [Discord4J](https://discord4j.com/) entwickelt und sein Quellcode ist �ffentlich"
					+ " auf [Bitbucket zu finden](https://bitbucket.org/geniuscoding/genius-discord-bot). (Open Source Software)");
			char delimiter = GeniusDiscordUtility.getDelimiter();
			for(DiscordCommandInfo c : map.keySet()) {
				embed.appendField(delimiter + c.getSyntax(), c.getHelptext() + "\n**Ab Rolle:** " + c.getPermission().toString(), false);
			}
			RequestBuffer.request(new IRequest<IMessage>() {

				@Override
				public IMessage request() {
					try {
						IMessage msg = e.getAuthor().getOrCreatePMChannel().sendMessage(embed.build());
						e.getChannel().sendMessage(":white_check_mark: Der Hilfswal ist auf dem Weg zu dir! :whale:");
						return msg;
					} catch(DiscordException ex) {
						e.getChannel().sendMessage(":x: Es scheint so, als k�nnte der Hilfswal nicht zu dir! :whale:");
						return null;
					}
				}
			});
		} else {
			sendInvalidArgsMessage(e);
		}
	}

}
