package com.genius.discordbot.commands;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

@CommandSubscriber(syntax = "quit", help = "F�hrt den Bot herunter.", permission = Permission.ADMINISTRATOR)
public class QuitCommand extends DiscordCommand {

	private final Logger logger;
	
	public QuitCommand(GeniusBot bot) {
		super(bot);
		this.logger = LoggerFactory.getLogger(QuitCommand.class);
	}

	@Override
	public void handle(final MessageReceivedEvent e, final List<String> args) {
		if(args.size() == 0) {
			e.getChannel().sendMessage(":white_check_mark: Der Bot wird beendet.");
			this.logger.info("User \"" + e.getAuthor().getName() + "\" dispatched the " 
							+ GeniusDiscordUtility.getDelimiter()
							+ this.getClass().getAnnotation(CommandSubscriber.class).syntax().split(" ")[0]
							+ " command. Shutting down...");
			try {
				this.getBot().getSqlconnection().getConnection().close();
			} catch (SQLException | NullPointerException ex) {
				logger.warn("Unable to close SQL connection gracefully, shutting down forcefully.");
				logger.warn(ex.getMessage());
			}
			this.getBot().getClient().logout();
			System.exit(0);
		} else {
			sendInvalidArgsMessage(e);
		}
	}

}
