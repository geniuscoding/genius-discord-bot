package com.genius.discordbot.commands;

import com.genius.discordbot.permission.Permission;

public final class DiscordCommandInfo { // POJO/VO just for saving syntax, perms & help text
	
	private final String name;
	private final String syntax;
	private final String helptext;
	private final Permission permission;
	
	public DiscordCommandInfo(final String name, final String syntax, final String helptext, final Permission permission) {
		this.name = name;
		this.syntax = syntax;
		this.helptext = helptext;
		this.permission = permission;
	}

	public String getName() {
		return name;
	}

	public String getSyntax() {
		return syntax;
	}

	public String getHelptext() {
		return helptext;
	}
	
	public Permission getPermission() {
		return permission;
	}
}