package com.genius.discordbot.commands;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.MessageTokenizer;
import sx.blah.discord.util.MessageTokenizer.ChannelMentionToken;
import sx.blah.discord.util.MessageTokenizer.MentionToken;
import sx.blah.discord.util.RateLimitException;

@CommandSubscriber(syntax = "config <Key> <Wert>", help = "Bietet RT-Zugriff auf die Konfiguration des Bots.", permission = Permission.ADMINISTRATOR)
public class ConfigCommand extends DiscordCommand {

	public ConfigCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if(args.size() == 2) {
			String key = args.get(0).toLowerCase();
			try {
				if(key.startsWith("channel")) {
					handleChannel(e, args);
				} else if(key.equals("bot.delimiter")) {
					handleBotDelimiter(e, args);
				} else if(key.equals("bot.status")) {
					handleBotStatus(e, args);
				} else if(key.equals("bot.username")) {
					handleUsername(e, args);
				} else if(key.equals("bot.avatar")) {
					handleAvatar(e, args);
				} else {
					e.getChannel().sendMessage(":x: Unbekannter Key!");
				}
			} catch(RateLimitException ex) {
				e.getChannel().sendMessage(":x: Rate limited! Bitte versuche es sp�ter erneut.");
			}
		} else {
			sendInvalidArgsMessage(e);
		}
	}
	
	private void handleChannel(MessageReceivedEvent e, List<String> args) {
		MessageTokenizer mt = new MessageTokenizer(e.getMessage());
		mt.nextWord();
		mt.nextWord();
		MentionToken<?> ment;
		if(mt.hasNextMention() && (ment = mt.nextMention()) instanceof ChannelMentionToken) {
			IChannel channel = (IChannel) ment.getMentionObject();
			GeniusBot.getConfig().store(args.get(0), channel.getLongID());
			e.getChannel().sendMessage(":white_check_mark: Wert gespeichert.");
		} else {
			e.getChannel().sendMessage(":x: Dieser Key ben�tigt als Value einen Channel!");
		}
	}
	
	private void handleBotDelimiter(MessageReceivedEvent e, List<String> args) {
		String key = args.get(0);
		String val = args.get(1);
		if(val.length() == 1) {
			GeniusBot.getConfig().store(key, val.charAt(0));
			e.getChannel().sendMessage(":white_check_mark: Delimiter zu \"" + val + "\" ge�ndert!");
		} else {
			e.getChannel().sendMessage(":x: Ung�ltiger Delimiter!");
		}
	}
	
	private void handleBotStatus(MessageReceivedEvent e, List<String> args) {
		String[] arsp1 = args.get(1).split(";");
		if(arsp1.length == 2) {
			arsp1[0] = arsp1[0].toLowerCase();
			ActivityType at = null;
			switch(arsp1[0]) {
			case "playing":
				at = ActivityType.PLAYING;
				break;
			case "listening":
				at = ActivityType.LISTENING;
				break;
			case "watching":
				at = ActivityType.WATCHING;
			case "?":
				break;
			default:
				e.getChannel().sendMessage(":x: Dieser Aktivit�tstyp existiert nicht!");
				return;
			}
			this.getBot().getClient().changePresence(StatusType.ONLINE, at, arsp1[1]);
			e.getChannel().sendMessage(":white_check_mark: Aktivit�t ge�ndert!");
		} else {
			e.getChannel().sendMessage(":x: Ung�ltiges Format! Syntax: <playing/listening/watching/?>;<text>");
		}
	}
	
	private void handleUsername(MessageReceivedEvent e, List<String> args) {
		try {
			this.getBot().getClient().changeUsername(args.get(1));
			e.getChannel().sendMessage(":white_check_mark: Nutzername ge�ndert!");
		} catch(DiscordException ex) {
			e.getChannel().sendMessage(":x: Konnte Nutzername nicht �ndern: " + ex.getMessage());
		}
	}
	
	private void handleAvatar(MessageReceivedEvent e, List<String> args) {
		URL imgurl;
		try {
			imgurl = new URL(args.get(1));
		} catch(MalformedURLException ex) {
			e.getChannel().sendMessage(":x: Ung�ltige Bild-URL!");
			return;
		}
		try {
			this.getBot().getClient().changeAvatar(Image.forUrl("png", imgurl.toString()));
			e.getChannel().sendMessage(":white_check_mark: Avatar ge�ndert!");
		} catch(DiscordException ex) {
			e.getChannel().sendMessage(":x: Konnte Avatar-Bild nicht �ndern: " + ex.getMessage());
		}
		
	}

}
