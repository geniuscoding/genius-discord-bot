package com.genius.discordbot.commands;

import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public abstract class DiscordCommand {
	
	private final GeniusBot bot;
	
	public DiscordCommand(final GeniusBot bot) {
		this.bot = bot;
	}

	public abstract void handle(final MessageReceivedEvent e, final List<String> args);
	
	public void sendInvalidArgsMessage(final MessageReceivedEvent e) {
		CommandSubscriber anno = this.getClass().getAnnotation(CommandSubscriber.class);
		e.getChannel().sendMessage(":x: Falsche Argumente! Syntax: " + GeniusDiscordUtility.getDelimiter() + anno.syntax());
	}
	
	public GeniusBot getBot() {
		return bot;
	}
	
}