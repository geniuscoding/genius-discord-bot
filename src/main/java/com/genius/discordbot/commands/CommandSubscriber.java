package com.genius.discordbot.commands;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.genius.discordbot.permission.Permission;

@Retention(RUNTIME)
@Target(TYPE)
public @interface CommandSubscriber {
	public String syntax();
	public String help() default "Kein Hilfstext.";
	public Permission permission() default Permission.CONTRIBUTOR;
	
}
