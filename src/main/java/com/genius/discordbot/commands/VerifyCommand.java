package com.genius.discordbot.commands;

import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.MessageTokenizer;
import sx.blah.discord.util.MessageTokenizer.MentionToken;
import sx.blah.discord.util.MessageTokenizer.UserMentionToken;

@CommandSubscriber(syntax = "verify <user>", help = "Verifiziert einen Nutzer und gibt ihm die Contributor-Rolle.", permission = Permission.EDITOR)
public class VerifyCommand extends DiscordCommand {

	public VerifyCommand(GeniusBot bot) {
		super(bot);
	}

	// TODO: Automate verification process using Genius REST API & Bot user account (cf. UpdateInitiatorPooler)
	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if(args.size() == 1) {
			MessageTokenizer mt = new MessageTokenizer(e.getMessage());
			mt.nextWord();
			MentionToken<?> ment;
			if(mt.hasNextMention() && (ment = mt.nextMention()) instanceof UserMentionToken) {
				IUser user = (IUser) ment.getMentionObject();
				IGuild gd = this.getBot().getClient().getGuildByID(GeniusDiscordUtility.getGeniusGuildID());
				IRole guestrole = gd.getRoleByID(GeniusDiscordUtility.getGuestRole());
				if(user.hasRole(guestrole)) {
					user.removeRole(guestrole);
					user.addRole(gd.getRoleByID(GeniusDiscordUtility.getContributorRole()));
					e.getChannel().sendMessage(":white_check_mark: Nutzer verifiziert.");
				} else {
					e.getChannel().sendMessage(":x: Dieser Nutzer muss nicht mehr verifziert werden!");
				}
			} else {
				e.getChannel().sendMessage(":x: Ungültiger Nutzer!");
			}
		} else {
			sendInvalidArgsMessage(e);
		}
	}

}
