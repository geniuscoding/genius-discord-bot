package com.genius.discordbot.commands;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.permission.Permission;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

@CommandSubscriber(syntax = "status", help = "Gibt Informationen �ber den Status des Bots.", permission = Permission.MODERATOR)
public class StatusCommand extends DiscordCommand {

	public StatusCommand(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(MessageReceivedEvent e, List<String> args) {
		if(args.size() == 0) {
			EmbedBuilder embed = new EmbedBuilder();
			boolean sql = this.getBot().getSqlconnection().hasFailed();
			embed.withAuthorIcon(this.getBot().getClient().getOurUser().getAvatarURL());
			embed.withAuthorUrl("https://bitbucket.org/geniuscoding/genius-discord-bot");
			embed.withAuthorName(this.getBot().getClient().getOurUser().getName() + ": Status");
			embed.appendField("Gestartet am", DateTimeFormatter.RFC_1123_DATE_TIME.format(this.getBot().getLaunchTimestamp()), true);
			embed.appendField("Ausgef�hrt f�r", FormatDuration(), true);
			embed.appendField("Server l�uft seit", getServerUptime(), false);
			embed.appendField("Datenbankverbindung", sql ? "Nein" : "Ja", false);
			embed.withColor(sql ? Color.RED : Color.GREEN);
			embed.withFooterText("made with :heart: by genius deutschland");
			
			RequestBuffer.request(() -> e.getChannel().sendMessage(embed.build()));
		} else {
			sendInvalidArgsMessage(e);
		}
	}
	
	private String FormatDuration() {
		ZonedDateTime cur = ZonedDateTime.now();
		Duration dur = Duration.between(cur, this.getBot().getLaunchTimestamp());
		long s = dur.getSeconds();
		//TODO: Replace Math.abs()...
		return String.format("%d:%02d:%02d", Math.abs(s / 3600), Math.abs((s % 3600) / 60), Math.abs((s % 60)));		
	}
	
	private String getServerUptime() {
		StringBuilder builder = new StringBuilder();
		try {
			Process p = Runtime.getRuntime().exec("uptime");
			p.waitFor();
			try(BufferedReader buf = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
				String line;
				while ((line = buf.readLine()) != null) {
				  builder.append(line);
				}
			}
			p.destroy();
		} catch (IOException | InterruptedException e) {
			builder.delete(0, builder.length());
			builder.append("Unbekannt");
		}
		String output = builder.toString();
		// TODO: <> Tage
		if(!output.equals("Unbekannt")) {
			return output.split(" ")[3] + " Tagen, " + output.split(" ")[1].split(":")[0] + " Stunden, " + output.split(" ")[1].split(":")[1] + " Minuten und " + output.split(" ")[1].split(":")[2] + " Sekunden";
		} else {
			return output;
		}
	}

}
