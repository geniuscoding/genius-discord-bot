package com.genius.discordbot;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		if(args.length != 3) {
			System.err.println("Invalid parameters! Syntax: <arg1:discordtoken> <arg2:geniustoken> <arg3:dbpass>");
			System.exit(-1);
			return;
		}
		
		Config fs;
		try {
			fs = new Config(new File("genius.cfg"));
		} catch (IllegalArgumentException | IOException e) {
			System.err.println("Could not create/load config file: " + e.getMessage());
			e.printStackTrace();
			System.exit(-2);
			return;
		}
		
		GeniusBot bot = new GeniusBot(args[0], args[1], args[2], fs);
		new Thread(bot, "Genius Bot Thread").start();
		new Thread(new UpdateInitiatorPooler(bot), "Updater Thread").start();
	}

}
