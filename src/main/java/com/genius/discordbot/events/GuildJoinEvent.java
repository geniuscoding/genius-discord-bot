package com.genius.discordbot.events;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;

import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;

public class GuildJoinEvent extends DiscordEvent<GuildCreateEvent> {

	public GuildJoinEvent(GeniusBot bot) {
		super(bot);
	}

	@Override
	public void handle(final GuildCreateEvent e) {
		if(e.getGuild().getLongID() != GeniusDiscordUtility.getGeniusGuildID()) {
			e.getGuild().leave();
		}
	}

}
