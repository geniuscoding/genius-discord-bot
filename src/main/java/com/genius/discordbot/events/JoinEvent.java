package com.genius.discordbot.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;

import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

@EventSubscriber
public class JoinEvent extends DiscordEvent<UserJoinEvent> {

	private Logger logger;
	
	public JoinEvent(GeniusBot bot) {
		super(bot);
		logger = LoggerFactory.getLogger(JoinEvent.class);
	}

	@Override
	public void handle(UserJoinEvent e) {
		IGuild gdg = this.getBot().getClient().getGuildByID(GeniusDiscordUtility.getGeniusGuildID());
		IUser user = e.getUser();
		int roleCount = user.getRolesForGuild(gdg).size();
    	IRole guestRole = this.getBot().getClient().getGuildByID(GeniusDiscordUtility.getGeniusGuildID()).getRoleByID(GeniusDiscordUtility.getGuestRole());
    	
//    	RoleCount > 1 = Role assigned; RoleCont == 1 = only @everyone
    	if (roleCount == 1) {
    		try {
				user.addRole(guestRole);
				gdg.getChannelByID(GeniusDiscordUtility.getEingangshalleChannelID()).sendMessage("Willkommen auf dem Genius Deutschland Discord-Server, " + user.mention() + "!");
			} catch (MissingPermissionsException | RateLimitException | DiscordException ex) {
				logger.warn("Unable to grant user \"" + user.getName() + "\" the Guest role or send a message in the welcome channel: " + ex.getMessage());
				ex.printStackTrace();
			}
    	}
	}

}
