package com.genius.discordbot.events;

import com.genius.discordbot.GeniusBot;

import sx.blah.discord.api.events.IListener;

public abstract class DiscordEvent<T extends sx.blah.discord.api.events.Event> implements IListener<T> {

	private final GeniusBot bot;
	
	public DiscordEvent(final GeniusBot bot) {
		this.bot = bot;
	}

	@Override
	public abstract void handle(T e);
	
	public GeniusBot getBot() {
		return bot;
	}

}
