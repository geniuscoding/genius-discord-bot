package com.genius.discordbot.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.genius.discordbot.GeniusBot;
import com.genius.discordbot.GeniusDiscordUtility;
import com.genius.discordbot.commands.CommandSubscriber;
import com.genius.discordbot.commands.DiscordCommand;
import com.genius.discordbot.commands.DiscordCommandInfo;
import com.genius.discordbot.permission.Permission;
import com.genius.discordbot.permission.PermissionUtils;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.MissingPermissionsException;

@EventSubscriber
public class IncomingMessageEvent extends DiscordEvent<MessageReceivedEvent>{

	private Logger logger;
	
	public IncomingMessageEvent(GeniusBot bot) {
		super(bot);
		logger = LoggerFactory.getLogger(IncomingMessageEvent.class);
	}

	@Override
	public void handle(MessageReceivedEvent e) {
		if(e.getAuthor() != this.getBot().getClient().getOurUser()) {
			if(!e.getChannel().isPrivate()) {
				if(e.getMessage().getContent().startsWith("" + GeniusDiscordUtility.getDelimiter())) {
					handleCommand(e);
				}
			}
		}
	}
	
	private void handleCommand(final MessageReceivedEvent e) {
		String[] split = e.getMessage().getContent().split(" ");
		split[0] = split[0].substring(1, split[0].length());
		
		Map<DiscordCommandInfo, DiscordCommand> commands = this.getBot().getCommands();
		for(DiscordCommandInfo c : commands.keySet()) {
			if(split[0].equals(c.getName())) {
				List<String> args = new ArrayList<>();
				for(int i = 1; i < split.length; i++) {
					args.add(split[i]);
				}
				DiscordCommand command = commands.get(c);
				if(canExecute(e, command)) {
					try {
						command.handle(e, args);
					} catch(MissingPermissionsException ex) {
						StringBuilder sb = new StringBuilder();
						ex.getMissingPermissions().forEach(p -> {
							sb.append(p.toString() + " ");
						});
						logger.warn("Can't execute command " + command.getClass().getSimpleName() + "; lacking the following permissions: " + sb.toString());
						ex.printStackTrace();
					}
				} else {
					e.getChannel().sendMessage(":x: Du darfst diesen Befehl nicht ausf�hren!");
				}
				return;
			}
		}
	}
	
	private boolean canExecute(final MessageReceivedEvent e, final DiscordCommand command) {
		IUser user = e.getAuthor();
		Permission reqPermission = command.getClass().getAnnotation(CommandSubscriber.class).permission();
		Permission highestPerm = Permission.GUEST;
		for(IRole r : user.getRolesForGuild(this.getBot().getClient().getGuildByID(GeniusDiscordUtility.getGeniusGuildID()))) {
			for(Permission p : Permission.values()) {
				if(r.getLongID() == p.getId() && PermissionUtils.getHigherPermission(p, highestPerm)) {
					highestPerm = p;
				}
			}
		}
		return PermissionUtils.getHigherPermission(highestPerm, reqPermission);
	}

}
