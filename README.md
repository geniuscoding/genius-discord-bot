# The almighty Genius Deutschland Discord Bot

yow, da der [alte discord bot](https://bitbucket.org/lars96/geniusbot/src/master/) ein gammeliges stück müll war (noch mehr als der hier), kommt hier der recode rein.
das meiste kann vom alten repo übernommen werden, allerdings kann man sich einige features und funktionen sparen einfach wegen der vorhandenen redundancy und um schlicht software bloating zu vermeiden.

hier im bot gibt's ne klasse mit dem namen _GeniusDiscordUtility_ (bzw. `com.genius.discordbot.utility.GeniusDiscordUtility`) für die mich ein durchschnittlicher software engineer vermutlich der extrem statischen anwendung des codes in die hölle schicken würde.
da wir den bot aber sowieso nur auf unserem discord nutzen, geht das schon klar. 8) oder wir sagen einfach, dass der bot nicht von uns ist und der code einfach irgendwie magisch erschienen ist. ¯\_(ツ)_/¯

jedenfalls sind in der klasse die ganzen ids für channels und rollen, damit der bot nicht für jede scheiße die änderbar oder eben nicht änderbar ist, eine anfrage an die DB senden muss.
deshalb: falls es weiteren konstanten stuff gibt: der soll da rein, yeah

---

wichtig: eigentlich sollte maven jetzt so konfiguriert sein, dass man das repo nur clonen muss und ein einfaches `maven install` reicht, damit alle dependencies im lokalen maven repo vorhanden sind
für die config des bots wird aber ein [mini-lib von debukkit](https://github.com/DeBukkIt/SimpleFileStorage) genutzt, welches es so nicht auf maven central oder sonst wo gibt

deswegen muss man es für maven auf die harte tour machen und das ding zuerst manuell installieren – was natürlich wie sonst auch so in der softwareentwicklung 'n ~~ziemlicher pain in the ass ist~~:

* SimpleFileStorage 1.2.0 [hier](https://github.com/DeBukkIt/SimpleFileStorage/releases/download/1.2.0/SimpleFileStorage-1.2.0.jar) runterladen
* maven das ding installieren lassen, dafür den befehl in die konsole tippseln: `mvn install:install-file -Dfile=<Pfad zur SimpleFileStorage-1.2.0.jar> -DgroupId=com.blogspot.debukkitsblog -DartifactId=SimpleFileStorage -Dversion=1.2.0-Dpackaging=jar -DgeneratePom=true`

wenn das geschafft ist, sollte eigentlich alles funktionieren und man kann loslegen, alles noch viel schlimmer zu machen, als es schon ist >:)
